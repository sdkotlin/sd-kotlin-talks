[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sdkotlin_sd-kotlin-talks&metric=alert_status)](https://sonarcloud.io/dashboard?id=sdkotlin_sd-kotlin-talks)

# San Diego Kotlin User Group Talks

Code examples from the [San Diego Kotlin User Group](https://www.meetup.com/sd-kotlin/) meetings on the  [Kotlin](http://kotlinlang.org/) programming language.
